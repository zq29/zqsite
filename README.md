# zqSite - My Personal Website

An easy-to-migrate, static html website, built with docker & git

Check [www.qianzuncheng.com](http://www.qianzuncheng.com) for a demo

## Design Goals

* Web server must be easy to migrate

  If you've been trying different VPSs for your website, like me, you must hate server migration

* Blog contents and server code must be well decoupled

  I am not writing for only publishing them on the web. I write anywhere, creating any kind of format like `.docx`, `.md`, and publish parts of them. So I wish I can write freely without having to adapt to how the server works

* Auto content backup

  The worst thing is to have a server crash without any backup

* Backward compatibility

  My previous web serves static html pages, and I wish the server could continue to do so.

## How the Whole Thing Works

Here's an overview of the whole system:

```mermaid
graph LR
subgraph Author
    A[awesomeProjectReport.docx]
    B[githubReadme.md]
    C[importantDocument.pdf]
    D[...]
    E(<strong>Converter</strong>)
    A-->E
    B-->E
    C-->E
    D-->E
    F[Static HTMLs]
end

E-->F

subgraph Server
    G(Web Server)
end

F-->G

subgraph Clients
    H(Visitor A)
    I(Visitor B)
    J(...)
end

G-->H
G-->I
G-->J
```

* This repo is for server only, check **[TODO]** for `Author` side deployment

* Docker makes migration easier than ever!

  Anywhere you deploy this server, just run

  ```shell
  docker-compose up
  ```

  then it's done! Just as easy as that!

  If you go check `zqSite/Dockerfile`, you will know this image is built from `ubuntu:16.04` and has:

  * `git`: for version control and content publishing (connects contents with server)
  * `apache2`: the static HTML web server

* Use static HTML as a layer between your writing and server code

## How the Server Works

Here's an overview of the server architecture:

```mermaid
graph LR
a(Author)
subgraph Server
	A("Git Server (hook)")
	B(Apache Web Server)
	C["/var/www/html"]
	A-- "git clone/pull" -->C
	C-->B
end
b(Visitors)
a-- "git push" -->A
B-->b
```

* There's really not much magic in this `apache2`, it just serves static html, that's all. This is kind of old-school, but enough for a personal website, **what is important is the content of this website**
* What's interesting is how the git server updates to `/var/www/html`, this is implemented by git hook `post-receive`, see `zqSite/githooks/post-receive`. For hooks, see [here](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks). Every time git server receives a push, it updates the repo and runs a script that updates to `/var/www` as well
* Check `zqSite/setup.sh` for more detail about how this server is setup

## There's "Nothing" in the Server!

The server has nothing but code, and yes, it does holds contents (static HTMLs), but it's NOT responsible for keeping the contents from losing! This is kind of against traditional server design, but this is just a static blog! And remember our design goals, contents and server are different part of the system. **You should keep your blogs safe, the server just presents them to visitors!**

See **[TODO]**, for the magic that happens at your side.

So when the server crashes, no worries, just kill it and use another 2 minutes to deploy it again!

## Deploy the Server

1. Install [docker](https://docs.docker.com/engine/install/), [docker-compose](https://docs.docker.com/compose/install/), [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

2. Get a copy of this repo

   ```shell
   git clone git@gitlab.oit.duke.edu:zq29/zqsite.git
   cd zqsite
   ```

3. Put your public ssh key in file `zqSite/gitkey.pub`, so that the file looks something like this:

   ```
   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG0W8365m/gM2ftVFufqV+qfVaY8nOjSRqty33gJsnop zq29@duke.edu
   ```

   See [here](https://docs.gitlab.com/ee/ssh/) for more information about git and ssh

   For example, if your public key is in file `~/.ssh/id_ed25519.pub`, you can do:

   ```shell
   cat ~/.ssh/id_ed25519.pub >> zqSite/gitkey.pub
   ```

4. Run the server!

   ```shell
   chmod 755 zqSite/setup.sh
   sudo docker-compose up -d
   ```

5. Now let's publish something to your web, see [here](https://gitlab.oit.duke.edu/zq29/zqauthor)
